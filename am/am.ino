#define INPUT_PIN  0 //Pin de Entrada ADC

#define TIMER_PIN  3 // Pin de salida PWM

#define DEBUG_PIN  2 // Pin de frecuencia de muestreo

#define LED_PIN   13 // displays input overdrive

#define SHIFT_BY   3 // 2 ... 7 Entrada del atenuador

#define TIMER_TOP 20 // Determina la frecuencia de la portadora

#define A_MAX TIMER_TOP / 4

float  OCR2B  = 0,OCR2A  = 0;
int    TCCR2A = 0,TCCR2B = 0;

void setup() {
  
  // Modo de los pines
    pinMode( DEBUG_PIN, OUTPUT );

    pinMode( TIMER_PIN, OUTPUT );

    pinMode( LED_PIN, OUTPUT );

    // set ADC prescaler to 16 to decrease conversion time (0b100)
    //establecer prescaler ADC a 16 para disminuir el tiempo de conversión
    ADCSRA = ( ADCSRA | _BV( ADPS2 ) ) & ~( _BV( ADPS1 ) | _BV( ADPS0 ) );

    // non-inverting; fast PWM with TOP; no prescaling
   
    TCCR2A = 0b10100011; // COM2A1 COM2A0 COM2B1 COM2B0 - - WGM21 WGM20

    TCCR2B = 0b00001001; // FOC2A FOC2B - - WGM22 CS22 CS21 CS20

    // 16E6 / ( OCR2A + 1 ) = 762 kHz @ TIMER_TOP = 20

    OCR2A = TIMER_TOP; //   = 727 kHz @ TIMER_TOP = 21

    OCR2B = TIMER_TOP / 2; // maximum carrier amplitude at 50% duty cycle

}



void loop() {

    // about 34 kHz sampling frequency
    //sobre los 34khz de frecuencia de muestreo
    digitalWrite( DEBUG_PIN, HIGH );

    int8_t value = ( analogRead( INPUT_PIN ) >> SHIFT_BY ) - ( 1 << ( 9 - SHIFT_BY ) );

    digitalWrite( DEBUG_PIN, LOW );

    // clipping : recortando los picos de la senal

    if ( value < -A_MAX ) {

        value = -A_MAX;

        digitalWrite( LED_PIN, HIGH );

    } else if ( value > A_MAX ) {
        
        value = A_MAX;

        digitalWrite( LED_PIN, HIGH );

    } else {

        digitalWrite( LED_PIN, LOW );

    }

    OCR2B = A_MAX + value;
}